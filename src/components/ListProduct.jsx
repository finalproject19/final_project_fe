import { UserAuth } from "../context/AuthContext";
import { CardImg } from "react-bootstrap";
import { FiSearch } from "react-icons/fi";
import {
  CardBody,
  CardSubtitle,
  CardTitle,
} from "reactstrap";
import { Link } from "react-router-dom";
import NavbarMenu1 from "./Navbar1";
import "../pages/ProductUpdate.scss";

const ListProduct = () => {
  const { getUserDatabase } = UserAuth();

  return (
    <div>
      <NavbarMenu1 />
      <div className="container mt-5">
        <div className="mx-4 list-2">
          <h3 className="mb-4 list-21">Daftar Jual Saya</h3>

          {/* user card */}
          <div className="card" style={{ borderRadius: "15px" }}>
            <CardBody>
              <div
                className="d-flex"
                style={{ justifyContent: "space-between" }}
              >
                <div className="d-flex" style={{ padding: "0.3rem" }}>
                  <CardImg
                    src={
                      getUserDatabase().image
                        ? getUserDatabase().image
                        : "https://icon-library.com/images/anonymous-person-icon/anonymous-person-icon-18.jpg"
                    }
                    style={{ width: "80px", borderRadius: "15px" }}
                  />
                  <CardBody>
                    <CardTitle tag="h5">{getUserDatabase().name}</CardTitle>
                    <CardSubtitle className="text-muted" tag="h6">
                      {getUserDatabase().city}
                    </CardSubtitle>
                  </CardBody>
                </div>
                <div style={{ padding: "0.3rem", marginTop: "1rem" }}>
                  <Link to="/profile">
                    <button
                      type="button"
                      style={{ marginRight: "10px" }}
                      className="btn tombol masuk"
                    >
                      Edit
                    </button>
                  </Link>
                </div>
              </div>
            </CardBody>
          </div>

          <div className="over list-3 mt-3">
            <button
              type="button"
              className="btn kategori"
              style={{ marginRight: "10px", minWidth: "fit-content" }}
            >
              <FiSearch className="me-2" style={{ fontSize: "20px" }} />
              Masuk
            </button>
            <button
              type="button"
              className="btn kategori"
              style={{ marginRight: "10px", minWidth: "fit-content" }}
            >
              <FiSearch className="me-2" style={{ fontSize: "20px" }} />
              Hobi
            </button>
            <button
              type="button"
              className="btn kategori"
              style={{ marginRight: "10px", minWidth: "fit-content" }}
            >
              <FiSearch className="me-2" style={{ fontSize: "20px" }} />
              Kendaraan
            </button>
            <button
              type="button"
              className="btn kategori"
              style={{ marginRight: "10px", minWidth: "fit-content" }}
            >
              <FiSearch className="me-2" style={{ fontSize: "20px" }} />
              Baju
            </button>
            <button
              type="button"
              className="btn kategori"
              style={{ marginRight: "10px", minWidth: "fit-content" }}
            >
              <FiSearch className="me-2" style={{ fontSize: "20px" }} />
              Elektronik
            </button>
            <button
              type="button"
              className="btn kategori"
              style={{ marginRight: "10px", minWidth: "fit-content" }}
            >
              <FiSearch className="me-2" style={{ fontSize: "20px" }} />
              Kesehatan
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ListProduct;
